Quickly spin up a MediaWiki instance with the CampaignEvents extension.

## Installation 

Ensure you have [Docker Desktop](https://www.docker.com/products/docker-desktop) installed.

Clone this repo:

    git clone https://gitlab.wikimedia.org/mhurd/mediawiki-campaignevents-docker.git

## Usage

Switch to the `mediawiki-campaignevents-docker` directory:

    cd ~/mediawiki-campaignevents-docker

Use this command to spin up MediaWiki with the [CampaignEvents](https://www.mediawiki.org/wiki/Extension:CampaignEvents) extension:
-   ```
    make
     ```
    - Fetches the latest MediaWiki into `~/mediawiki-campaignevents-docker/mediawiki-docker-make/mediawiki/`
    - Fetches the latest CampaignEvents extension into `~/mediawiki-campaignevents-docker/mediawiki-docker-make/mediawiki/extensions/CampaignEvents`
    - Configures MediaWiki to use the CampaignEvents extension
    - Spins up Docker containment to serve MediaWiki pages and provide CampaignEvents special page
    - Opens CampaignEvents special page ( Note: you can log in using 'Admin' and 'dockerpass' )

See this [Makefile](https://gitlab.wikimedia.org/mhurd/mediawiki-docker-make) for other supported make commands.