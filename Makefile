SHELL := /bin/bash

.DEFAULT: freshinstallwithcampaignevents
.PHONY: freshinstallwithcampaignevents
freshinstallwithcampaignevents:
	-@if [ ! -f "./mediawiki-docker-make/Makefile" ]; then \
		git submodule update --init; \
	fi
#	The include file should now be in place, so subsequent "make" calls will have access to the include vars
	make continueinstallation;

CREATE_EVENT_URL = "http://localhost:8080/w/index.php?title=Special:UserLogin&returnto=Special:EnableEventRegistration"

.PHONY: continueinstallation
continueinstallation:
	make freshinstall skipopenspecialversionpage=true;
	make applyextension extensionDirectory=CampaignEvents extensionRepoURL=https://gerrit.wikimedia.org/r/mediawiki/extensions/CampaignEvents wfLoadExtension=CampaignEvents;
	cd $(mediawiki_dir); \
	docker compose exec mediawiki-jobrunner /bin/bash -c "cd maintenance && php update.php;"
	@echo -e "\nOpening '$(CREATE_EVENT_URL)'.\nLog in using 'Admin' and 'dockerpass' to view the CampaignEvents special page.\n";
	$(makefile_dir)/utility.sh open_url_when_available $(CREATE_EVENT_URL);

-include ./mediawiki-docker-make/Makefile